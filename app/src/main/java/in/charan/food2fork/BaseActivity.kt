package `in`.charan.food2fork

import android.view.View
import android.widget.FrameLayout
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout

/**
Author: Charan Kumar
Date: 2019-10-08
 */
abstract class BaseActivity: AppCompatActivity() {

    private lateinit var progressBar: ProgressBar

    override fun setContentView(layoutResID: Int) {

        val constraintLayout = layoutInflater.inflate(R.layout.activity_base, null) as ConstraintLayout
        val frameLayout4ActivityContent: FrameLayout = constraintLayout.findViewById(R.id.abActivityContent)

        progressBar = constraintLayout.findViewById(R.id.abProgressBarLoading)

        layoutInflater.inflate(layoutResID, frameLayout4ActivityContent, true)

        super.setContentView(constraintLayout)
    }

    protected fun toggleVisibility() {
        if (progressBar.visibility == View.VISIBLE) {
            progressBar.visibility = View.GONE

        } else {
            progressBar.visibility = View.VISIBLE
        }
    }
}