package `in`.charan.food2fork.adapters

import `in`.charan.food2fork.R
import `in`.charan.food2fork.models.Recipe
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import de.hdodenhof.circleimageview.CircleImageView
import kotlin.math.roundToInt

/**
Author: Charan Kumar
Date: 2019-11-12
 */
class CategoryViewHolder(
    private val categoryView: View,
    private val onRecipeListener: OnRecipeListener
): RecyclerView.ViewHolder(categoryView),
    View.OnClickListener {

    private var tvTitle: TextView = categoryView.findViewById(R.id.category_title)
    private var ivImage: CircleImageView = categoryView.findViewById(R.id.category_image)
    private var categoryName: String = ""

    init {
        categoryView.setOnClickListener(this)
    }


    override fun onClick(view: View?) {
        onRecipeListener.onCategoryClicked(categoryName)
    }

    fun setData(recipe: Recipe, requestOptions: RequestOptions) {
        categoryName = recipe.title
        tvTitle.text = recipe.title

        Glide.with(itemView.context)
            .setDefaultRequestOptions(requestOptions)
            .load(categoryView.context.resources.getDrawable(R.drawable.italian))
            .into(ivImage)
    }

}