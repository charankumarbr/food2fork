package `in`.charan.food2fork.adapters

import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
Author: Charan Kumar
Date: 2019-11-12
 */
open class LoadingViewHolder(private val loadingView: View): RecyclerView.ViewHolder(loadingView)


class PaginationViewHolder(private val paginationView: View): LoadingViewHolder(paginationView)