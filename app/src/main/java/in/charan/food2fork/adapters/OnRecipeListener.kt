package `in`.charan.food2fork.adapters

/**
Author: Charan Kumar
Date: 2019-11-11
 */
interface OnRecipeListener {

    fun onRecipeClicked(position: Int)

    fun onCategoryClicked(categoryName: String)

    fun onNextPage()

}