package `in`.charan.food2fork.adapters

import `in`.charan.food2fork.R
import `in`.charan.food2fork.models.Recipe
import `in`.charan.food2fork.util.Constants
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.request.RequestOptions

/**
Author: Charan Kumar
Date: 2019-11-02
 */
class RecipeRecyclerAdapter (private val onRecipeListener: OnRecipeListener):
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TYPE_RECIPE = 1
    private val TYPE_LOADING = 2
    private val TYPE_CATEGORY = 3
    private val TYPE_PAGINATION = 4

    private val LOADING_TITLE = "LOADING..."
    private val CATEGORY_RANKING = -1F

    private var recipes: MutableList<Recipe>? = null

    private val requestOptions = RequestOptions().placeholder(R.drawable.ic_launcher_background)

    private val isLoading = false
    private var isLoadingRequired = false

    override fun getItemViewType(position: Int): Int {
        return if (position >= recipes?.size!!) {
            TYPE_PAGINATION

        } else {
            when {
                recipes!![position].social_rank == CATEGORY_RANKING -> TYPE_CATEGORY
                recipes!![position].title == LOADING_TITLE -> TYPE_LOADING
                else -> TYPE_RECIPE
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            TYPE_RECIPE -> {
                val recipeView = LayoutInflater.from(parent.context).
                    inflate(R.layout.layout_recipe_list_item, parent, false)
                return RecipeViewHolder(recipeView, onRecipeListener)
            }

            TYPE_LOADING -> {
                val loadingView = LayoutInflater.from(parent.context).
                    inflate(R.layout.layout_loading_list_item, parent, false)
                return LoadingViewHolder(loadingView)
            }

            TYPE_CATEGORY -> {
                val categoryView = LayoutInflater.from(parent.context).
                    inflate(R.layout.layout_category_list_item, parent, false)
                return CategoryViewHolder(categoryView, onRecipeListener)
            }

            TYPE_PAGINATION -> {
                val loadingView = LayoutInflater.from(parent.context).
                    inflate(R.layout.layout_loading_list_item, parent, false)
                return PaginationViewHolder(loadingView)
            }

            else -> {
                val loadingView = LayoutInflater.from(parent.context).
                    inflate(R.layout.layout_loading_list_item, parent, false)
                return LoadingViewHolder(loadingView)
            }
        }
    }

    override fun getItemCount(): Int {
        val count = recipes?.size?.plus(if (isLoadingRequired &&
            recipes!!.size > 0) { 1 } else { 0 } ) ?: 0
        return count
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is RecipeViewHolder -> holder.setData(recipes!![position], requestOptions)
            is CategoryViewHolder -> holder.setData(recipes!![position], requestOptions)
            is PaginationViewHolder -> onRecipeListener.onNextPage()
        }
    }

    fun setRecipes(recipes: List<Recipe>) {
        this.recipes = recipes.toMutableList()
        notifyDataSetChanged()
    }

    fun displayLoading() {
        if (!isPageOneLoading()) {
            val loadingRecipeList = mutableListOf(getLoadingRecipe())
            setRecipes(loadingRecipeList)
        }
    }

    fun displayCategories() {
        val categories = mutableListOf<Recipe>()
        for (index in Constants.DEFAULT_SEARCH_CATEGORIES.indices) {
            val recipe = Recipe()
            recipe.title = Constants.DEFAULT_SEARCH_CATEGORIES[index]
            recipe.social_rank = CATEGORY_RANKING
            categories.add(recipe)
        }
        isLoadingRequired = false
        setRecipes(categories)
    }

    private fun getLoadingRecipe(): Recipe {
        return Recipe().apply {
            title = LOADING_TITLE
        }
    }

    private fun isPageOneLoading(): Boolean {
        recipes?.let {
            return if (it.size > 0) {
                it[it.size - 1].title == LOADING_TITLE
            } else {
                false
            }
        }

        return false
    }

    fun setLoadingRequired(b: Boolean) {
        isLoadingRequired = b
        notifyDataSetChanged()
    }

    fun getSelectedRecipe(position: Int): Recipe {
        return recipes!![position]
    }
}