package `in`.charan.food2fork.adapters

import `in`.charan.food2fork.R
import `in`.charan.food2fork.models.Recipe
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlin.math.roundToInt

/**
Author: Charan Kumar
Date: 2019-11-11
 */
class RecipeViewHolder(private val recipeView: View,
                       private val onRecipeListener: OnRecipeListener):
    RecyclerView.ViewHolder(recipeView),
    View.OnClickListener {

    private var tvTitle: TextView = recipeView.findViewById(R.id.recipe_title)
    private var tvPublisher: TextView = recipeView.findViewById(R.id.recipe_publisher)
    private var tvSocialScore: TextView = recipeView.findViewById(R.id.recipe_social_score)

    private var ivImage: AppCompatImageView = recipeView.findViewById(R.id.recipe_image)

    init {
        recipeView.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        onRecipeListener.onRecipeClicked(adapterPosition)
    }

    fun setData(
        recipe: Recipe,
        requestOptions: RequestOptions) {
        tvTitle.text = recipe.title
        tvPublisher.text = recipe.publisher
        tvSocialScore.text = recipe.social_rank.roundToInt().toString()

        Glide.with(itemView.context)
            .setDefaultRequestOptions(requestOptions)
            .load(recipe.image_url)
            .into(ivImage)
    }
}