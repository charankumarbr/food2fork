package `in`.charan.food2fork.customviews

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
Author: Charan Kumar
Date: 2019-11-13
 */
class VerticalSpacingItemDecorator (private val verticalSpacingHeight: Int): RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)
        outRect.top = verticalSpacingHeight
    }

}