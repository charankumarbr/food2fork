package `in`.charan.food2fork.models

import android.os.Parcel
import android.os.Parcelable

/**
Author: Charan Kumar
Date: 2019-10-09
 */
data class Recipe constructor(val recipe_id: String,
                              var title: String,
                              val publisher: String,
                              val ingredients: Array<String>?,
                              val image_url: String,
                              var social_rank: Float): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.readString()!!,
        parcel.createStringArray()!!,
        parcel.readString()!!,
        parcel.readFloat()
    )

    constructor():this("","","", arrayOf(""),
        "", 0F)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Recipe

        if (recipe_id != other.recipe_id) return false
        if (title != other.title) return false
        if (publisher != other.publisher) return false
        /*if (!ingredients?.contentEquals(other.ingredients?)) return false*/
        if (image_url != other.image_url) return false
        if (social_rank != other.social_rank) return false

        return true
    }

    override fun hashCode(): Int {
        var result = recipe_id.hashCode()
        result = 31 * result + title.hashCode()
        result = 31 * result + publisher.hashCode()
        ingredients?.let {
            result = 31 * result + it.contentHashCode()
        }
        result = 31 * result + image_url.hashCode()
        result = 31 * result + social_rank.hashCode()
        return result
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(recipe_id)
        parcel.writeString(title)
        parcel.writeString(publisher)
        parcel.writeStringArray(ingredients)
        parcel.writeString(image_url)
        parcel.writeFloat(social_rank)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Recipe> {
        override fun createFromParcel(parcel: Parcel): Recipe {
            return Recipe(parcel)
        }

        override fun newArray(size: Int): Array<Recipe?> {
            return arrayOfNulls(size)
        }
    }
}