package `in`.charan.food2fork.network

import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService

/**
Author: Charan Kumar
Date: 2019-10-27
 */
class AppExecutors private constructor() {

    val networkIO: ScheduledExecutorService = Executors.
        newScheduledThreadPool(3)

    companion object {
        private val appExecutors = AppExecutors()

        fun getInstance() = appExecutors
    }
}