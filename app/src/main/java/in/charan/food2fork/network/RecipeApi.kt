package `in`.charan.food2fork.network

import `in`.charan.food2fork.requests.responses.RecipeListResponse
import `in`.charan.food2fork.requests.responses.RecipeResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

/**
Author: Charan Kumar
Date: 2019-10-09
 */
interface RecipeApi {

    //SEARCH
    @GET("api/search")
    fun searchRecipe(@Query("key") key: String,
                     @Query("q") query: String?,
                     @Query("page") page: String): Call<RecipeListResponse>

    // GET RECIPE
    @GET("api/get")
    fun getRecipe(@Query("key") key: String,
                  @Query("rId") recipeId: String): Call<RecipeResponse>

    @GET
    fun getFromUrl(@Url url: String): Call<String>

}