package `in`.charan.food2fork.network

import `in`.charan.food2fork.util.Constants
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.squareup.moshi.Moshi
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

/**
Author: Charan Kumar
Date: 2019-10-09
 */

class Retrofit {

    companion object {

        private fun getRetrofitInstance() = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create().asLenient())
            .build()

        fun recipeApi() = getRetrofitInstance().create(RecipeApi::class.java)

        private fun getUrlRetrofitInstance() = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(ScalarsConverterFactory.create())
            .build()

        fun urlApi() = getUrlRetrofitInstance().create(RecipeApi::class.java)
    }
}