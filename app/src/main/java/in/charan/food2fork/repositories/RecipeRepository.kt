package `in`.charan.food2fork.repositories

import `in`.charan.food2fork.models.Recipe
import `in`.charan.food2fork.requests.RecipeApiClient
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

/**
Author: Charan Kumar
Date: 2019-10-18
 */
class RecipeRepository private constructor() {

    private val recipeApiClient = RecipeApiClient.getInstance()

    fun getRecipes(): LiveData<List<Recipe>> = recipeApiClient.getRecipes()

    private var query: String? = null
    private var page: Int = 0

    fun getRecipe(): LiveData<Recipe> = recipeApiClient.getRecipe()

    fun searchRecipesApi(query: String, pageNumber: Int) {
        val page = if (pageNumber == 0) {
            1
        } else {
            pageNumber
        }
        this.query = query
        this.page = pageNumber
        recipeApiClient.searchRecipesApi(query, page)
    }

    fun getUrlData() = recipeApiClient.getUrlData()
    /*fun grabFromURL(url: String) {
        recipeApiClient.grabFromUrl(url)
    }*/

    fun searchNextPage() {
        recipeApiClient.searchRecipesApi(query!!, page + 1)
    }

    fun getRecipe(recipeId: String) {
        recipeApiClient.getRecipe(recipeId)
    }


    companion object {
        private val recipeRepository = RecipeRepository()
        fun getInstance() = recipeRepository
    }
}