package `in`.charan.food2fork.requests

import `in`.charan.food2fork.models.Recipe
import `in`.charan.food2fork.network.AppExecutors
import `in`.charan.food2fork.network.Retrofit
import `in`.charan.food2fork.requests.responses.RecipeListResponse
import `in`.charan.food2fork.requests.responses.RecipeResponse
import `in`.charan.food2fork.util.Constants
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.TimeUnit

/**
Author: Charan Kumar
Date: 2019-10-18
 */
class RecipeApiClient private constructor() {

    private var recipes: MutableLiveData<List<Recipe>> = MutableLiveData()
    fun getRecipes(): LiveData<List<Recipe>> = recipes
    private var retrieveRecipesRunnable: RetrieveRecipesRunnable? = null

    private var recipe: MutableLiveData<Recipe> = MutableLiveData()
    fun getRecipe(): LiveData<Recipe> = recipe
    private var retrieveRecipeRunnable: RetrieveRecipeRunnable? = null

    private var urlData: MutableLiveData<String> = MutableLiveData()
    fun getUrlData(): LiveData<String> = urlData

    companion object {
        private val recipeApiClient = RecipeApiClient()
        fun getInstance() = recipeApiClient
    }

    fun searchRecipesApi(query: String, pageNumber: Int) {

        if (retrieveRecipesRunnable != null) {
            retrieveRecipesRunnable = null
        }
        retrieveRecipesRunnable = RetrieveRecipesRunnable(pageNumber, query)
        val futureHandler = AppExecutors.getInstance().networkIO.submit(retrieveRecipesRunnable)

        AppExecutors.getInstance().networkIO.schedule({
            // request timed out
            futureHandler.cancel(true)

        }, Constants.NETWORK_TIMEOUT, TimeUnit.MILLISECONDS)
    }

    private inner class RetrieveRecipesRunnable constructor(val pageNumber: Int,
                                       val query: String): Runnable {

        private var cancelRequest = false

        override fun run() {
            try {
                val response = getRecipes(pageNumber, query).execute()
                if (cancelRequest) {
                    return
                }

                if (response.isSuccessful) {
                    val recipeListResponse = (response.body() as RecipeListResponse)
                    val list = ArrayList<Recipe>(recipeListResponse.recipes!!)

                    if (pageNumber == 1) {
                        recipes.postValue(list)

                    } else {
                        val currentRecipes = recipes.value as MutableList<Recipe>
                        currentRecipes.addAll(list)
                        recipes.postValue(currentRecipes)
                    }

                } else {
                    val error = response.errorBody().toString()
                    Log.e("RecipeApiClient", "Error:: $error")
                    recipes.postValue(null)
                }
            } catch (exception: Exception) {
                Log.e("RecipeApiClient", "Error", exception)
                recipes.postValue(null)
            }
        }

        private fun getRecipes(pageNumber: Int, query: String): Call<RecipeListResponse> {
            return Retrofit.recipeApi().searchRecipe(Constants.API_KEY,
                query,
                pageNumber.toString())
        }

        fun cancelRequest() {
            cancelRequest = true
        }

    }



    fun getRecipe(recipeId: String) {
        if (retrieveRecipeRunnable != null) {
            retrieveRecipeRunnable = null
        }
        retrieveRecipeRunnable = RetrieveRecipeRunnable(recipeId)
        val futureHandler = AppExecutors.getInstance().networkIO.submit(retrieveRecipeRunnable)

        AppExecutors.getInstance().networkIO.schedule({
            // request timed out
            futureHandler.cancel(true)

        }, Constants.NETWORK_TIMEOUT, TimeUnit.MILLISECONDS)
    }

    private inner class RetrieveRecipeRunnable constructor(val recipeId: String): Runnable {

        private var cancelRequest = false

        override fun run() {
            try {
                val response = getRecipeRequest(recipeId).execute()
                if (cancelRequest) {
                    return
                }

                if (response.isSuccessful) {
                    val recipeListResponse = (response.body() as RecipeResponse)
                    recipe.postValue(recipeListResponse.recipe)

                } else {
                    val error = response.errorBody().toString()
                    Log.e("RecipeApiClient", "Error:: $error")
                    recipe.postValue(null)
                }
            } catch (exception: Exception) {
                Log.e("RecipeApiClient", "Error", exception)
                recipe.postValue(null)
            }
        }

        private fun getRecipeRequest(query: String): Call<RecipeResponse> {
            return Retrofit.recipeApi().getRecipe(Constants.API_KEY,
                query)
        }

        fun cancelRequest() {
            cancelRequest = true
        }

    }
}