package `in`.charan.food2fork.requests.responses

import `in`.charan.food2fork.models.Recipe
import com.squareup.moshi.Json

/**
Author: Charan Kumar
Date: 2019-10-09
 */
data class RecipeListResponse constructor(@Json(name = "count")
                                          var count: Int = 0,
                                          @Json(name = "recipes")
                                          var recipes: List<Recipe>? = null)