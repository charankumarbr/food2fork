package `in`.charan.food2fork.requests.responses

import `in`.charan.food2fork.models.Recipe
import com.squareup.moshi.Json

/**
Author: Charan Kumar
Date: 2019-10-09
 */
class RecipeResponse {

    @Json(name = "recipe")
    var recipe: Recipe? = null

}