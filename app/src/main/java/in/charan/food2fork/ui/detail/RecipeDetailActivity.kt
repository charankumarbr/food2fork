package `in`.charan.food2fork.ui.detail

import `in`.charan.food2fork.R
import `in`.charan.food2fork.models.Recipe
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_recipe_detail.*

class RecipeDetailActivity : AppCompatActivity() {

    private lateinit var recipeDetailViewModel: RecipeDetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe_detail)

        recipeDetailViewModel = ViewModelProviders.of(this).get(RecipeDetailViewModel::class.java)
        recipeDetailViewModel.getRecipe().observe(this, Observer {
            it?.let {
                if (it.recipe_id == recipeDetailViewModel.recipeId) {
                    displayData(it)
                }
            }
        })
        getIncomingIntent()
    }

    private fun displayData(recipe: Recipe) {
        recipe_title.text = recipe.title
        recipe_social_score.text = recipe.social_rank.toString()

        Glide.with(this)
            .setDefaultRequestOptions(RequestOptions().placeholder(R.drawable.ic_launcher_background))
            .load(recipe.image_url)
            .into(recipe_image)

        val builder = StringBuilder()
        recipe.ingredients!!.forEach {
            builder.append(it)
            builder.append("\n")
        }
        ingredients_container.text = builder.toString()

        ardPbLoading.clearAnimation()
        ardPbLoading.visibility = View.GONE

        ardSvLayout.visibility = View.VISIBLE
    }

    private fun getIncomingIntent() {
        if (intent.hasExtra("recipe")) {
            val recipe = intent.getParcelableExtra<Recipe>("recipe")
            recipe!!.ingredients
            recipeDetailViewModel.getRecipe(recipe.recipe_id)

        } else {
            finish()
        }
    }
}
