package `in`.charan.food2fork.ui.detail

import `in`.charan.food2fork.models.Recipe
import `in`.charan.food2fork.repositories.RecipeRepository
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel

/**
Author: Charan Kumar
Date: 2019-11-19
 */
class RecipeDetailViewModel: ViewModel() {

    var recipeId: String = ""
    private val recipeRepository = RecipeRepository.getInstance()

    fun getRecipe(): LiveData<Recipe> = recipeRepository.getRecipe()

    fun getRecipe(recipeId: String) {
        this.recipeId = recipeId
        recipeRepository.getRecipe(recipeId)
    }
}