package `in`.charan.food2fork.ui.list

import `in`.charan.food2fork.BaseActivity
import `in`.charan.food2fork.R
import `in`.charan.food2fork.adapters.OnRecipeListener
import `in`.charan.food2fork.adapters.RecipeRecyclerAdapter
import `in`.charan.food2fork.customviews.VerticalSpacingItemDecorator
import `in`.charan.food2fork.network.Retrofit
import `in`.charan.food2fork.requests.responses.RecipeResponse
import `in`.charan.food2fork.ui.detail.RecipeDetailActivity
import `in`.charan.food2fork.util.Constants
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_recipe_list.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RecipeListActivity : BaseActivity(), OnRecipeListener {

    private lateinit var recipeListViewModel: RecipeListViewModel

    private lateinit var recipeListAdapter: RecipeRecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recipe_list)

        recipeListViewModel = ViewModelProviders.of(this).get(RecipeListViewModel::class.java)

        initRecyclerView()
        initSearchView()
        subscribeObservers()

        if (!recipeListViewModel.isViewingRecipes) {
            displaySearchCategories()
        }

        /*arlButton.setOnClickListener {
            toggleVisibility()
            testSearchApi()
        }*/

        //testSearchApi()
        //testGetRecipeApi()

    }

    private fun displaySearchCategories() {
        recipeListViewModel.isViewingRecipes = false
        recipeListAdapter.displayCategories()
    }

    private fun initRecyclerView() {
        recipeListAdapter = RecipeRecyclerAdapter(this)
        arlRecipeList.addItemDecoration(VerticalSpacingItemDecorator(6))
        arlRecipeList.layoutManager = LinearLayoutManager(this)
        arlRecipeList.adapter = recipeListAdapter
    }

    private fun initSearchView() {
        arlSearch.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let {
                    searchRecipesApi(it, 1)
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
             return false
            }
        })
    }

    private fun subscribeObservers() {
        recipeListViewModel.getRecipes().observe(this, Observer { recipes ->
            recipes?.let {
                if (recipeListViewModel.isViewingRecipes) {
                    it.forEach { recipe ->
                        Log.d("RecipeList", recipe.title)
                    }
                    recipeListAdapter.setRecipes(it)
                    if (it.isEmpty()) {
                        recipeListAdapter.setLoadingRequired(false)
                    } else {
                        recipeListAdapter.setLoadingRequired(true)
                    }
                }
            }
        })

        recipeListViewModel.getUrlData().observe(this, Observer { urlData ->
            urlData?.let {
                Log.d("URLData", urlData)
            }
        })
    }

    private fun testGetRecipeApi() {
        val call = Retrofit.recipeApi().getRecipe(
            Constants.API_KEY,
            "8c0314"
        )
        call.enqueue(object: Callback<RecipeResponse> {

            override fun onFailure(call: Call<RecipeResponse>, t: Throwable) {

            }

            override fun onResponse(
                call: Call<RecipeResponse>,
                response: Response<RecipeResponse>) {
                if (response.isSuccessful) {
                    Log.d("Recipe", "Success ${response.body()}")
                    response.body()?.recipe?.let {
                        Log.d("Recipe", it.title)
                    }
                }
            }
        })
    }

    private fun testSearchApi() {

        searchRecipesApi("chicken breast", 0)

        /*val call = Retrofit.recipeApi().searchRecipe(
            Constants.API_KEY,
            "bread",
            "1"
        )
        call.enqueue(object: Callback<RecipeListResponse> {

            override fun onFailure(call: Call<RecipeListResponse>, t: Throwable) {

            }

            override fun onResponse(
                call: Call<RecipeListResponse>,
                response: Response<RecipeListResponse>) {
                if (response.isSuccessful) {
                    Log.d("RecipeList", "Success ${response.body()}")
                    val recipes = response.body()?.recipes
                    recipes?.forEach {
                        Log.d("RecipeList", it.title)
                    }
                }
            }
        })*/
    }

    private fun searchRecipesApi(query: String, pageNumber: Int) {
        recipeListAdapter.displayLoading()
        recipeListViewModel.searchRecipesApi(query, pageNumber)
    }

    override fun onRecipeClicked(position: Int) {
        val selectedRecipe = recipeListAdapter.getSelectedRecipe(position)
        val intent = Intent(this, RecipeDetailActivity::class.java)
        intent.putExtra("recipe", selectedRecipe)
        startActivity(intent)
    }

    override fun onCategoryClicked(categoryName: String) {
        searchRecipesApi(categoryName, 1)
    }

    override fun onNextPage() {
        recipeListViewModel.searchNextPage()
    }

    override fun onBackPressed() {
        if (recipeListViewModel.onBackPressed()) {
            super.onBackPressed()
        } else {
            displaySearchCategories()
        }
    }
}
