package `in`.charan.food2fork.ui.list

import `in`.charan.food2fork.models.Recipe
import `in`.charan.food2fork.repositories.RecipeRepository
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

/**
Author: Charan Kumar
Date: 2019-10-10
 */
class RecipeListViewModel: ViewModel() {

    var isViewingRecipes = false

    private val recipeRepository = RecipeRepository.getInstance()

    fun getRecipes(): LiveData<List<Recipe>> = recipeRepository.getRecipes()

    fun searchRecipesApi(query: String, pageNumber: Int) {
        isViewingRecipes = true
        recipeRepository.searchRecipesApi(query, pageNumber)
        //recipeRepository.grabFromURL("https://blog.truecaller.com/2018/01/22/life-as-an-android-engineer/")
    }

    fun onBackPressed(): Boolean {
        return !isViewingRecipes
    }

    fun getUrlData() = recipeRepository.getUrlData()
    fun searchNextPage() {
        recipeRepository.searchNextPage()
    }

}