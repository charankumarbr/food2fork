package `in`.charan.food2fork.util

/**
Author: Charan Kumar
Date: 2019-10-08
 */
class Constants {

    companion object {

        val BASE_URL = "https://www.food2fork.com/"

        val API_KEY = "09c39edf3e23f72e841d9fcba81c9923"

        val NETWORK_TIMEOUT = 50000L

        val DEFAULT_SEARCH_CATEGORIES = arrayOf(
            "Barbeque",
            "Breakfast",
            "Chicken",
            "Beef",
            "Brunch",
            "Dinner",
            "Wine",
            "Italian"
        )

        /*val DEFAULT_SEARCH_CATEGORY_IMAGES = arrayOf(
            "barbeque",
            "breakfast",
            "chicken",
            "beef",
            "brunch",
            "dinner",
            "wine",
            "italian"
        )*/
    }
}